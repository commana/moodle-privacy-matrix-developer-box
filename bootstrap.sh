#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get install -y apache2 git-core mysql-server php5 php5-curl php5-mysql php5-intl php5-xmlrpc php5-gd php5-xdebug
rm -rf /var/www
ln -fs /vagrant /var/www

cd /opt
mkdir -p moodledata
chown www-data:www-data moodledata

# Enable prettier xdebug output
perl -pi -e 's/^html_errors\s*=\s*Off/html_errors = On/' /etc/php5/apache2/php.ini

#
# MySQL Setup
#
cat /vagrant/moodle.sql | mysql -u root

#
# Moodle Setup
#
cp /vagrant/config.php /var/www/moodle/config.php




#
# Finish up ...
#
service apache2 restart
