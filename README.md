# Privacy Matrix Developer Box

## Overview

This project sets up a complete environment needed to develop Moodle and it's plugin Privacy Matrix.

It uses Vagrant and VirtualBox to set up the environment.

## Usage

Essentially, there are three steps:

1. Set up the box itself. This is done via `vagrant up`.
2. Check out Moodle as well as the Privacy Matrix (`./checkout.sh`). The sources are kept outside the virtual machine so you may destroy your box without affecting your Moodle sources.
3. Visit Moodle at <http://localhost:4567/moodle> and follow the instructions.

## Setup

Your box starts with an empty Moodle that has not been installed yet. This is intentional, so you're able to start with a fresh Moodle any time you clean up your machine. The configuration file has already been put in place, so you do not need to worry about any database credentials. All you have to do is to accept the license agreement, and finish the database setup. Finally, you're asked to configure your admin account.

## Internals

In case you want to modify your box, you may want to have a look at the following files:

* **Vagrantfile**: the virtual machine's base configuration.
* **bootstrap.sh**: installs and configures all required software to run Moodle as well as Moodle itself.
* **checkout.sh**: checks out Moodle and the Privacy Matrix report plugin.
* **config.php**: the Moodle configuration that is copied into the Moodle installation. This file includes the database credentials.
* **moodle.sql**: commands to create the Moodle database and its user.

## Is it any good?

Yes.
